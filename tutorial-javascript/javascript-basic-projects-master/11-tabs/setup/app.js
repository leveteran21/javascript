const btns = document.querySelectorAll('.tab-btn');
const about = document.querySelector('.about');
const articles = document.querySelectorAll('.content');

console.log(btns);
console.log(about);
console.log(articles);

about.addEventListener("click", function(event) {
    //console.log(event.target.dataset.id);
    const id = event.target.dataset.id; // je récupère la dataset id
    if (id) {
        btns.forEach(function(btn) {
            btn.classList.remove("active");
            event.target.classList.add("active");
        });
        articles.forEach(function(article) {
            // console.log(article.classList);
            article.classList.remove("active");
        })
        const element = document.getElementById(id);
        element.classList.add("active");
    }
});